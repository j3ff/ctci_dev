# Author: Jeff Szielenski
# Find max number of points that we can remove one at a time.
# Constraints: can remove one at a time, can only remove in
# row/column if there are more than one there.

# Global variable used to store max count.
max_count = 0

def find_max(pts):
    # Quick validation.
    if len(pts) == 0:
        return 0
    else:
        # Recurse.
        find_max_helper(pts, [], 0)

def find_max_helper(pts, chosen, count):
    global max_count

    # Base case.
    if len(pts) == 0:
        print("current max count: " + max_count)
    # Recursive case.
    else:
        # Try each point in the input array.
        for i,p in enumerate(pts):
            # Choose the point.
            c = p
            del pts[i]
            
            if is_valid(pts, c):
                # Meets constraints, can be removed.
                chosen.append(c)

                # Explore rest of points.
                find_max_helper(pts, chosen, count+1)
            
            # Unchoose point.
            if len(chosen) > 0:
                c = chosen.pop()
                pts.insert(0, c)

            # Keep track of max count.
            if count > max_count:
                # Store new max count.
                max_count = count
                # Debug print order of removal.
                chosen.append(c)
                print("chosen: ", chosen)

            # Reset count and continue with next point.
            count = 0

def is_valid(pts, p):
    for i,v in enumerate(pts):
        if p[0] == v[0] or p[0] == v[1] or p[1] == v[0] or p[1] == v[1]: 
            return True
    return False

# Unit Tests.

import unittest

class UnitTest(unittest.TestCase):
    # Example input given in meetup.
    def test1(self):
        global max_count
        max_count = 0
        pts = [[1,0], [0,2], [2,1], [1,3], [2,3], [0,4]]
        find_max(pts)
        self.assertEqual(max_count, 4)

    # Rearrange input from test 1.
    def test2(self):
        global max_count
        max_count = 0
        pts = [[2,1], [1,3], [1,0], [0,2], [2,3], [0,4]]
        find_max(pts)
        self.assertEqual(max_count, 4)

    # New test set I generated.
    def test3(self):
        global max_count
        max_count = 0
        pts = [[0,2], [0,4], [1,0], [2,1], [3,3], [1,3]]
        find_max(pts)
        self.assertEqual(max_count, 5)

unittest.main(verbosity=2)